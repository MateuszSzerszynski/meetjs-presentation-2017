registerAnimator('sync-scroller', class {

    constructor ({maxTime}) {
        this.maxTime = maxTime;
    }

    animate(currentTime, effect) {
        const breakpoint = 0.5 * this.maxTime;
        if (currentTime < 0.5 * this.maxTime) {
            effect.children[0].localTime = 0.5 * currentTime;
        } else {
            effect.children[0].localTime = breakpoint + 2 * (currentTime - breakpoint);
        }
    }
});