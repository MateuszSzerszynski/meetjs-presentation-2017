
window.animationWorkletPolyfill.addModule('sync-scroller.js').then(_ => {
    const content = document.getElementById('content');
    const previewContainer = document.getElementById('preview__container');
    const preview = previewContainer.getElementsByClassName('preview')[0];

    const scrollRange = content.scrollHeight;
    const scrollTimeline = new ScrollTimeline({
        scrollSource: content,
        orientation: 'vertical',
        timeRange: scrollRange
    });

    const duration = scrollRange + previewContainer.offsetHeight;
    const scrollEffect = new KeyframeEffect(
        preview,
        [   {transform: `translateY(0)`},
            {transform: `translateY(-${scrollRange}px)`}
        ],
        {duration});

    window.parallaxAnimator = new WorkletAnimation('sync-scroller',
        [scrollEffect],
        scrollTimeline, {
            maxTime: duration
        });
    window.parallaxAnimator.play();

});
