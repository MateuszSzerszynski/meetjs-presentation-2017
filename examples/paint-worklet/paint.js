registerPaint('card', class {

    /**
     * CSS Properties na zmianę których reaguje worklet.
     * W momencie ich zmiany przeglądarka wywoła funkcję paint
     */
    static get inputProperties() {
        return [
            '--frame',
            '--border-width',
            '--angle-size',
            '--first-color',
            '--second-color',
            '--border-color'
        ];
    }

    /**
     * Wywoływane w oddzielnym wątku przez przeglądarkę
     * gdy należy ponownie wyświetlić tło elementu
     *
     * Np. gdy zmienią się CSS Properties na które reaguje worklet,
     * zdefiniowane w 'inputProperties'
     *
     * @param ctx - obiekt po którym możemy rysować, dające API podobne do Canvas
     * @param geom - geometria obiektu do którego zainicjowany jest worklet,
     *                  określa jego wysokość i szerokość
     * @param properties    CSS properties z których korzysta worklet, pozwalające
     *                      na modyfikację wyświetlanego obiektu
     */
    paint(ctx, geom, properties) {
        const borderColor = properties.get('--border-color').toString();
        const borderWidth = this.getIntProperty(properties, '--border-width');
        const angle = this.getIntProperty(properties, '--angle-size');

        const width = geom.width;
        const height = geom.height;

        const backgroundColor = this.generateColor(ctx, properties, width, height);
        this.drawBackground(ctx, width, height, backgroundColor, angle, borderWidth);

        this.drawBorder(ctx, width, height, borderWidth, borderColor, angle);
    }

    /**
     * Funkcja odpowiedzialna za rysowanie tła
     */
    drawBackground(ctx, width, height, color, angle, borderWidth) {
        ctx.lineWidth = 1;
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(borderWidth, borderWidth);
        ctx.quadraticCurveTo(width/2, angle, width - borderWidth, borderWidth);
        ctx.quadraticCurveTo(width-angle, height/2, width - borderWidth, height - borderWidth);
        ctx.quadraticCurveTo(width/2, height-angle, borderWidth, height - borderWidth);
        ctx.quadraticCurveTo(angle, height/2, borderWidth + 1, borderWidth - 2);
        ctx.fill();
        ctx.closePath();
    }

    /**
     * Funkcja odpowiedzialna za rysowanie krawędzi
     */
    drawBorder(ctx, width, height, borderWidth, color, angle) {
        ctx.lineWidth = borderWidth;
        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(borderWidth, borderWidth);
        ctx.quadraticCurveTo(width/2, angle, width - borderWidth, borderWidth);
        ctx.quadraticCurveTo(width-angle, height/2, width - borderWidth, height - borderWidth);
        ctx.quadraticCurveTo(width/2, height-angle, borderWidth, height - borderWidth);
        ctx.quadraticCurveTo(angle, height/2, borderWidth + 1, borderWidth - 2);
        ctx.stroke();
        ctx.closePath();
    }

    getIntProperty(properties, name) {
        return parseInt(properties.get(name).toString());
    }

    /**
     * Generowanie koloru w zależności od klatki
     * zdefiniowanej w CSS Property '--frame'
     */
    generateColor(ctx, properties, width, height) {
        const frame = this.getIntProperty(properties, '--frame');

        const firstColor = properties.get('--first-color').toString();
        const secondColor = properties.get('--second-color').toString();

        const gradient = ctx.createLinearGradient(0, 0, width, height);
        gradient.addColorStop(0, firstColor);

        let step = frame / 1000.0;
        if (step > 1) {
            step = 1 - (step - 1);
        }
        gradient.addColorStop(step, secondColor);
        gradient.addColorStop(1, firstColor);

        return gradient;
    }

});