function cardElement() {
    return document.querySelector(".card");
}

// animation of border
let borderAnimation = false;
let animationStart;
document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#animation').addEventListener("change", () => {
        borderAnimation = !borderAnimation;

        if (borderAnimation) {
            animationStart = performance.now();
            animateBorder();
        } else {
            cardElement().style.setProperty('--frame', 0);
        }
    });
});

function animateBorder() {
    requestAnimationFrame((time) => {
        if (!borderAnimation) {
            return;
        }

        const frame = Math.max(parseInt((time - animationStart) * 2 % 2000), 0);
        cardElement().style.setProperty('--frame', frame);

        animateBorder();
    })
}

//border width
document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#angle-size').addEventListener('change', e => {
        cardElement().style.setProperty('--angle-size', e.target.value);
    });
});
